const srcImg = ['./img/img-1.png', './img/img-2.jpg', './img/img-3.jpg', './img/img-4.jpg', './img/img-5.jpg', './img/img-6.jpg'];
const btnPrevious = document.querySelector('.button-previous');
const btnNext = document.querySelector('.button-next');
const imgSlide = document.querySelector('img');

btnNext.addEventListener('click', function () {
    const imgNow = imgSlide.getAttribute('src');
    let imgIndex = srcImg.indexOf(imgNow) + 1;
    if(srcImg.indexOf(imgNow) === srcImg.length-1){
        imgIndex = 0;
    }
    imgSlide.setAttribute('src', `${srcImg[imgIndex]}`);
    console.log(srcImg[imgIndex])
});

btnPrevious.addEventListener('click', function () {
    const imgNow = imgSlide.getAttribute('src');
    let imgIndex = srcImg.indexOf(imgNow) - 1;
    if(srcImg.indexOf(imgNow) === 0){
        imgIndex = srcImg.length-1;
    }
    imgSlide.setAttribute('src', `${srcImg[imgIndex]}`);
    console.log(srcImg[imgIndex])
})
